package compiler.visitors;

import compiler.gen.GrammarBaseVisitor;
import compiler.gen.GrammarParser;

public class BlockVisitor extends GrammarBaseVisitor<Void> {

    @Override
    public Void visitBlock(GrammarParser.BlockContext ctx) {

        StatementVisitor statementVisitor = new StatementVisitor();

        for (int i = 0; i < ctx.statement().size(); i++) {
            statementVisitor.visit(ctx.statement(i));
        }

        return null;

    }

}
