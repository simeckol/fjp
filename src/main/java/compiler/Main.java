package compiler;

import java.io.*;

import compiler.gen.GrammarLexer;
import compiler.gen.GrammarParser;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.misc.ParseCancellationException;
import org.antlr.v4.runtime.tree.ParseTree;
import compiler.visitors.ProgramVisitor;

public class Main {

    public static void main(String[] args) {

        String path = null;

        if (args.length > 0) {
            path = args[0];

        } else {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Enter valid file path:");
            try {
                path = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            setupTree(path);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void setupTree(String path) throws IOException {
        CharStream charStream = CharStreams.fromFileName(path);
        GrammarLexer grammarLexer = new GrammarLexer(charStream);
        CommonTokenStream tokens = new CommonTokenStream(grammarLexer);
        GrammarParser grammarParser = new GrammarParser(tokens);

        ParseTree parseTree = grammarParser.program();

        System.out.println(parseTree.toStringTree(grammarParser));

        try {
            new ProgramVisitor().visit(parseTree);
        } catch (ParseCancellationException e) {
            System.err.println("Compile error: " + e.getMessage());
        }

    }

}
