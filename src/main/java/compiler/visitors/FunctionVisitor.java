package compiler.visitors;

import compiler.enums.Instruction;
import compiler.enums.VariableType;
import compiler.gen.GrammarBaseVisitor;
import compiler.gen.GrammarParser;
import compiler.ident_table.Function;
import compiler.ident_table.Variable;
import compiler.writer.Placeholder;
import org.antlr.v4.runtime.misc.ParseCancellationException;

import java.util.LinkedHashMap;
import java.util.List;

import static compiler.Common.*;

public class FunctionVisitor extends GrammarBaseVisitor<Void> {

    @Override
    public Void visitFunction(GrammarParser.FunctionContext ctx) {

        currentLevel++;
        currentFuncName = ctx.IDENT().getSymbol().getText();
        currentFuncArgsCount = ctx.argument().size();

        int firstIndex = instructionsWritten;

        String funcname = ctx.IDENT().getSymbol().getText();
        if (!functionMap.contains(funcname)) {

            declarationCount = 0;

            // skip declared variables
            Placeholder placeholder = writer.writeInstruction(Instruction.INT, 0);

            LinkedHashMap<String, VariableType> args = new LinkedHashMap<>();

            List<GrammarParser.ArgumentContext> argCtxs = ctx.argument();

            for (int i = 0; i < argCtxs.size(); i++) {
                GrammarParser.ArgumentContext curr = argCtxs.get(i);

                args.put(curr.IDENT().getSymbol().getText(), VariableType.getByRuleIndex(curr.type().opt.getType()));

                Variable var = new Variable(VariableType.getByRuleIndex(curr.type().opt.getType()), currentLevel, OFFSET + i);

                variableMap.add(currentFuncName, curr.IDENT().getSymbol().getText(), var);

                writer.writeInstruction(Instruction.LOD, currentLevel, OFFSET + RETURNOFFSET + i);
                writer.writeInstruction(Instruction.STO, 0, i + OFFSET);
            }

            Function function = new Function(firstIndex, VariableType.getByRuleIndex(ctx.type().opt.getType()), args);
            functionMap.add(funcname, function);

            new FuncBlockVisitor().visit(ctx.funcblock());

            maxArgumentsCount = Math.max(maxArgumentsCount, function.getArgs().size());

            int length = instructionsWritten - firstIndex;
            function.setLength(length);

            declarationCount = declarationCount + function.getArgs().size();

            // skip declared variables
            placeholder.fill(OFFSET + declarationCount + maxArgumentsCount + RETURNOFFSET);

            currentLevel--;

        } else {
            throw new ParseCancellationException(String.format("Function %s already exists.", funcname));
        }

        return null;
    }

}
