package compiler.enums;

import compiler.gen.GrammarParser;
import org.antlr.v4.runtime.misc.ParseCancellationException;

public enum OperationType {

    PLUS,
    MINUS,
    LOGAND,
    LOGOR,
    EQUALITY,
    INEQUALITY,
    GREATER,
    SMALLER;

    public static OperationType getByGrammarParserValue(int value) {
        switch (value) {
            case (GrammarParser.PLUS):
                return OperationType.PLUS;
            case (GrammarParser.MINUS):
                return OperationType.MINUS;
            case (GrammarParser.LOGAND):
                return OperationType.LOGAND;
            case (GrammarParser.LOGOR):
                return OperationType.LOGOR;
            case (GrammarParser.EQUALITY):
                return OperationType.EQUALITY;
            case (GrammarParser.INEQUALITY):
                return OperationType.INEQUALITY;
            case (GrammarParser.GREATER):
                return OperationType.GREATER;
            case (GrammarParser.SMALLER):
                return OperationType.SMALLER;
            default:
                throw new ParseCancellationException("No such operation type.");
        }
    }

}
