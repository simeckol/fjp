package compiler.writer;

import compiler.enums.Instruction;

import java.io.*;
import java.nio.charset.StandardCharsets;

import static compiler.Common.*;

public class OutWriter {
    private static final String COMPILED_FILE = "compiled.txt";
    private static final String COMPILED_TEMP_FILE = "compiled_temp.txt";
    private static OutWriter ourInstance = new OutWriter();

    private Writer fileWriter;
    private int nextAddress = 0;

    public static OutWriter getInstance() {
        return ourInstance;
    }

    private OutWriter() {

    }

    public void openWriter() {
        openWriter(false);
    }

    private void openWriter(boolean append) {
        try {
            fileWriter = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(COMPILED_FILE, append), StandardCharsets.UTF_8));
        } catch(Exception e) {
            System.out.println("Write initialization failed.");
        }
    }

    public void writeInstruction(Instruction instruction, int first, int second) {
        String instr = String.format("%d %s %d %d\n", nextAddress++, instruction, first, second);
        try {
            instructionsWritten++;
            fileWriter.write(instr);
        } catch (IOException e) {
            System.out.println("Write error.");
        }
    }

    public Placeholder writeInstruction(Instruction instruction, int first) {
        writeInstruction(instruction, first, -1);
        return new Placeholder(nextAddress - 1);
    }

    public void rewriteInstruction(int address, int second) {
        try {
            // close writer
            closeWriter();

            // create temp file
            Writer tempFileWriter = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(COMPILED_TEMP_FILE), StandardCharsets.UTF_8));

            // write to temp file till modified instruction
            BufferedReader br = new BufferedReader(new FileReader(COMPILED_FILE));
            int addressCounter = 0;
            for (String line; (line = br.readLine()) != null; addressCounter++) {
                if (addressCounter == address) {
                    String[] split = line.split(" ");
                    String instr = String.format("%s %s %s %d\n", split[0], split[1], split[2], second);
                    tempFileWriter.write(instr);
                    continue;
                }
                tempFileWriter.write(line + '\n');

            }

            // close temp file
            tempFileWriter.close();
            br.close();

            // remove original file
            File file = new File(COMPILED_FILE);
            boolean deleted = file.delete();
            if (!deleted) {
                System.out.println("Replace error.");
            }

            // rename temp file to original file
            file = new File(COMPILED_TEMP_FILE);
            File renameToFile = new File(COMPILED_FILE);
            boolean renamed = file.renameTo(renameToFile);
            if (!renamed) {
                System.out.println("Replace error.");
            }

            // open original file to append
            openWriter(true);

        } catch (IOException e) {
            System.out.println("Replace error.");
        }
    }

    public void closeWriter() {
        try {
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Close writer error.");
        }
    }
}
