// Generated from C:/Users/user/Documents/Projects/fjp/src/compiler\Grammar.g4 by ANTLR 4.7
package compiler.gen;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link GrammarParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface GrammarVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link GrammarParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram(GrammarParser.ProgramContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#functions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctions(GrammarParser.FunctionsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code declarationStatement}
	 * labeled alternative in {@link GrammarParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclarationStatement(GrammarParser.DeclarationStatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ifcaseStatement}
	 * labeled alternative in {@link GrammarParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfcaseStatement(GrammarParser.IfcaseStatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code switchcasesStatement}
	 * labeled alternative in {@link GrammarParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSwitchcasesStatement(GrammarParser.SwitchcasesStatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code assignStatement}
	 * labeled alternative in {@link GrammarParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignStatement(GrammarParser.AssignStatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code loopStatement}
	 * labeled alternative in {@link GrammarParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLoopStatement(GrammarParser.LoopStatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expressionStatement}
	 * labeled alternative in {@link GrammarParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpressionStatement(GrammarParser.ExpressionStatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code parallelassignStatement}
	 * labeled alternative in {@link GrammarParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParallelassignStatement(GrammarParser.ParallelassignStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#declaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclaration(GrammarParser.DeclarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#assign}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssign(GrammarParser.AssignContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#parallelassign}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParallelassign(GrammarParser.ParallelassignContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType(GrammarParser.TypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code negationExpression}
	 * labeled alternative in {@link GrammarParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNegationExpression(GrammarParser.NegationExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code comparationExpression}
	 * labeled alternative in {@link GrammarParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComparationExpression(GrammarParser.ComparationExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code funccallExpression}
	 * labeled alternative in {@link GrammarParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunccallExpression(GrammarParser.FunccallExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code factorExpression}
	 * labeled alternative in {@link GrammarParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFactorExpression(GrammarParser.FactorExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code parenthesedExpression}
	 * labeled alternative in {@link GrammarParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParenthesedExpression(GrammarParser.ParenthesedExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code priorityExpression}
	 * labeled alternative in {@link GrammarParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPriorityExpression(GrammarParser.PriorityExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code operationExpression}
	 * labeled alternative in {@link GrammarParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOperationExpression(GrammarParser.OperationExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ternaryExpression}
	 * labeled alternative in {@link GrammarParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTernaryExpression(GrammarParser.TernaryExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code logicalExpression}
	 * labeled alternative in {@link GrammarParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogicalExpression(GrammarParser.LogicalExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code signExpression}
	 * labeled alternative in {@link GrammarParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSignExpression(GrammarParser.SignExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#ifcase}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfcase(GrammarParser.IfcaseContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#funccall}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunccall(GrammarParser.FunccallContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#funcargs}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFuncargs(GrammarParser.FuncargsContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#ternary}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTernary(GrammarParser.TernaryContext ctx);
	/**
	 * Visit a parse tree produced by the {@code whileLoopLoop}
	 * labeled alternative in {@link GrammarParser#loop}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhileLoopLoop(GrammarParser.WhileLoopLoopContext ctx);
	/**
	 * Visit a parse tree produced by the {@code doWhileLoopLoop}
	 * labeled alternative in {@link GrammarParser#loop}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDoWhileLoopLoop(GrammarParser.DoWhileLoopLoopContext ctx);
	/**
	 * Visit a parse tree produced by the {@code repeatUntilLoopLoop}
	 * labeled alternative in {@link GrammarParser#loop}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRepeatUntilLoopLoop(GrammarParser.RepeatUntilLoopLoopContext ctx);
	/**
	 * Visit a parse tree produced by the {@code forLoopLoop}
	 * labeled alternative in {@link GrammarParser#loop}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForLoopLoop(GrammarParser.ForLoopLoopContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#whileloop}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhileloop(GrammarParser.WhileloopContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#dowhileloop}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDowhileloop(GrammarParser.DowhileloopContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#repeatuntilloop}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRepeatuntilloop(GrammarParser.RepeatuntilloopContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#forloop}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForloop(GrammarParser.ForloopContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlock(GrammarParser.BlockContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#funcblock}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFuncblock(GrammarParser.FuncblockContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#mainblock}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMainblock(GrammarParser.MainblockContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#function}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction(GrammarParser.FunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#argument}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArgument(GrammarParser.ArgumentContext ctx);
	/**
	 * Visit a parse tree produced by the {@code intvalueFactor}
	 * labeled alternative in {@link GrammarParser#factor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntvalueFactor(GrammarParser.IntvalueFactorContext ctx);
	/**
	 * Visit a parse tree produced by the {@code boolvalueFactor}
	 * labeled alternative in {@link GrammarParser#factor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolvalueFactor(GrammarParser.BoolvalueFactorContext ctx);
	/**
	 * Visit a parse tree produced by the {@code identFactor}
	 * labeled alternative in {@link GrammarParser#factor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdentFactor(GrammarParser.IdentFactorContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#switchcases}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSwitchcases(GrammarParser.SwitchcasesContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#incase}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIncase(GrammarParser.IncaseContext ctx);
}