package compiler.ident_table;

import java.util.HashMap;

public class FunctionMap {

    private static final FunctionMap instance = new FunctionMap();

    private HashMap<String, Function> map;

    private FunctionMap() {
        map = new HashMap<>();
    }

    public static FunctionMap getInstance() {
        return instance;
    }

    public Function get(String key) {
        return map.get(key);
    }

    public void add(String key, Function var) {
        map.put(key, var);
    }

    public boolean contains(String key) {
        return map.containsKey(key);
    }
}
