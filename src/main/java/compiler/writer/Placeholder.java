package compiler.writer;

import static compiler.Common.*;

public class Placeholder {
    private int address;

    public Placeholder(int address) {
        this.address = address;
    }

    public void fill(int value) {
        writer.rewriteInstruction(address, value);
    }
}
