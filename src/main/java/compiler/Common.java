package compiler;

import compiler.ident_table.FunctionMap;
import compiler.ident_table.VariableMap;
import compiler.writer.OutWriter;

public final class Common {

    public static int currentLevel = 0;

    public static String currentFuncName;

    public static int currentFuncArgsCount;

    public static OutWriter writer = OutWriter.getInstance();

    public static VariableMap variableMap = VariableMap.getInstance();

    public static FunctionMap functionMap = FunctionMap.getInstance();

    public static int declarationCount = 0;

    public static int instructionsWritten = 0;

    public static int maxArgumentsCount = 1;

    /** OFFSETS **/

    public static final int OFFSET = 3;

    public static final int RETURNOFFSET = 1;

    /** ERROR MESSAGES **/

    public static final String TYPE_ERROR_MSG = "Incorrect variable type %s. Expected %s.";

    public static final String OPERATOR_ERROR_MSG = "Unknown operator";

    public static final String CONDITION_ERROR_MSG = "Condition is not BOOL type.";

    public static final String VAR_NOT_FOUND_ERROR_MSG = "Variable %s not found in function %s.";

}
