package compiler.visitors;

import compiler.enums.Instruction;
import compiler.enums.Operation;
import compiler.enums.VariableType;
import compiler.gen.GrammarBaseVisitor;
import compiler.gen.GrammarParser;
import compiler.ident_table.Function;
import compiler.writer.Placeholder;
import org.antlr.v4.runtime.misc.ParseCancellationException;

import java.util.LinkedHashMap;
import java.util.Map;

import static compiler.Common.*;

public class ExpressionVisitor extends GrammarBaseVisitor<VariableType> {

    @Override
    public VariableType visitParenthesedExpression(GrammarParser.ParenthesedExpressionContext ctx) {
        return this.visit(ctx.expression());
    }

    @Override
    public VariableType visitFactorExpression(GrammarParser.FactorExpressionContext ctx) {
        return new FactorVisitor().visit(ctx);
    }

    @Override
    public VariableType visitNegationExpression(GrammarParser.NegationExpressionContext ctx) {
        VariableType type = this.visit(ctx.expression());

        if (type != VariableType.BOOL) {
            throw new ParseCancellationException(String.format(TYPE_ERROR_MSG, type, "BOOL"));
        }

        writer.writeInstruction(Instruction.LIT, 0, 0);
        writer.writeInstruction(Instruction.OPR, 0, Operation.EQ.code());

        return type;
    }

    @Override
    public VariableType visitFunccallExpression(GrammarParser.FunccallExpressionContext ctx) {
        return this.visit(ctx.funccall());
    }

    @Override
    public VariableType visitFunccall(GrammarParser.FunccallContext ctx) {
        String funcname = ctx.IDENT().getSymbol().getText();

        if (functionMap.contains(funcname)) {

            Function function = functionMap.get(ctx.IDENT().getSymbol().getText());
            LinkedHashMap<String, VariableType> funcargs = function.getArgs();

            int i = 0;
            for (Map.Entry<String, VariableType> entry : funcargs.entrySet()) {
                VariableType type = entry.getValue();

                GrammarParser.ExpressionContext arg = ctx.funcargs().expression().get(i);
                VariableType exp = this.visit(arg);

                if (!((exp == VariableType.CONSTINT || exp == VariableType.INT) && (type == VariableType.CONSTINT || type == VariableType.INT)) && exp != type) {
                    throw new ParseCancellationException(String.format("Wrong parameter type %s, expected %s.", exp, type));
                }

                writer.writeInstruction(Instruction.STO, 0, OFFSET + RETURNOFFSET + i);

                i++;
            }

            writer.writeInstruction(Instruction.CAL, 0, function.getIndex());

            if (function.getReturnType() != VariableType.VOID) {
                writer.writeInstruction(Instruction.LOD, 0, OFFSET);
            }
            return function.getReturnType();

        } else {
            throw new ParseCancellationException(String.format("Function %s not found.", funcname));
        }
    }

    @Override
    public VariableType visitOperationExpression(GrammarParser.OperationExpressionContext ctx) {

        VariableType exp1 = this.visit(ctx.expression(0));
        VariableType exp2 = this.visit(ctx.expression(1));

        if (exp1 != VariableType.INT && exp1 != VariableType.CONSTINT) {
            throw new ParseCancellationException(String.format(TYPE_ERROR_MSG, exp1, "INT or CONSTINT"));
        }

        if (exp2 != VariableType.INT && exp2 != VariableType.CONSTINT) {
            throw new ParseCancellationException(String.format(TYPE_ERROR_MSG, exp2, "INT or CONSTINT"));
        }

        switch (ctx.opt.getType()) {
            case (GrammarParser.PLUS):
                writer.writeInstruction(Instruction.OPR, 0, Operation.ADD.code());
                break;
            case (GrammarParser.MINUS):
                writer.writeInstruction(Instruction.OPR, 0, Operation.SUB.code());
                break;
            default:
                throw new ParseCancellationException(OPERATOR_ERROR_MSG);
        }

        return exp1;
    }

    @Override
    public VariableType visitSignExpression(GrammarParser.SignExpressionContext ctx) {

        VariableType exp = this.visit(ctx.expression());

        if (exp != VariableType.INT && exp != VariableType.CONSTINT) {
            throw new ParseCancellationException(String.format(TYPE_ERROR_MSG, exp, "INT or CONSTINT"));
        }

        if (ctx.opt.getType() == GrammarParser.MINUS) {
            writer.writeInstruction(Instruction.OPR, 0, Operation.NEG.code());
        }

        return exp;
    }

    @Override
    public VariableType visitPriorityExpression(GrammarParser.PriorityExpressionContext ctx) {

        VariableType exp1 = this.visit(ctx.expression(0));
        VariableType exp2 = this.visit(ctx.expression(1));

        if (exp1 != VariableType.INT && exp1 != VariableType.CONSTINT) {
            throw new ParseCancellationException(String.format(TYPE_ERROR_MSG, exp1, "INT or CONSTINT"));
        }

        if (exp2 != VariableType.INT && exp2 != VariableType.CONSTINT) {
            throw new ParseCancellationException(String.format(TYPE_ERROR_MSG, exp2, "INT or CONSTINT"));
        }

        switch (ctx.opt.getType()) {
            case (GrammarParser.TIMES):
                writer.writeInstruction(Instruction.OPR, 0, Operation.MUL.code());
                break;
            case (GrammarParser.DIVIDE):
                writer.writeInstruction(Instruction.OPR, 0, Operation.DIV.code());
                break;
            case (GrammarParser.MODULO):
                writer.writeInstruction(Instruction.OPR, 0, Operation.MOD.code());
                break;
            default:
                throw new ParseCancellationException(OPERATOR_ERROR_MSG);
        }

        return exp1;
    }

    @Override
    public VariableType visitLogicalExpression(GrammarParser.LogicalExpressionContext ctx) {

        VariableType exp1 = this.visit(ctx.expression(0));
        VariableType exp2 = this.visit(ctx.expression(1));

        if (exp1 != VariableType.BOOL) {
            throw new ParseCancellationException(String.format(TYPE_ERROR_MSG, exp1, "BOOL"));
        }

        if (exp2 != VariableType.BOOL) {
            throw new ParseCancellationException(String.format(TYPE_ERROR_MSG, exp2, "BOOL"));
        }

        writer.writeInstruction(Instruction.OPR, 0, Operation.ADD.code());

        switch (ctx.opt.getType()) {
            case (GrammarParser.LOGAND):
                writer.writeInstruction(Instruction.LIT, 0, 2);
                writer.writeInstruction(Instruction.OPR, 0, Operation.EQ.code());
                break;
            case (GrammarParser.LOGOR):
                writer.writeInstruction(Instruction.LIT, 0, 1);
                writer.writeInstruction(Instruction.OPR, 0, Operation.GE.code());
                break;
            default:
                throw new ParseCancellationException(OPERATOR_ERROR_MSG);
        }

        return VariableType.BOOL;
    }

    @Override
    public VariableType visitComparationExpression(GrammarParser.ComparationExpressionContext ctx) {

        VariableType exp1 = this.visit(ctx.expression(0));
        VariableType exp2 = this.visit(ctx.expression(1));

        if (exp1 != VariableType.INT && exp1 != VariableType.CONSTINT && exp2 != VariableType.INT && exp2 != VariableType.CONSTINT) {
            throw new ParseCancellationException(String.format(TYPE_ERROR_MSG, exp1, "INT or CONSTINT"));
        }

        switch (ctx.opt.getType()) {
            case (GrammarParser.EQUALITY):
                writer.writeInstruction(Instruction.OPR, 0, Operation.EQ.code());
                break;
            case (GrammarParser.INEQUALITY):
                writer.writeInstruction(Instruction.OPR, 0, Operation.NE.code());
                break;
            case (GrammarParser.GREATER):
                writer.writeInstruction(Instruction.OPR, 0, Operation.GT.code());
                break;
            case (GrammarParser.SMALLER):
                writer.writeInstruction(Instruction.OPR, 0, Operation.LT.code());
                break;
            default:
                throw new ParseCancellationException(OPERATOR_ERROR_MSG);
        }

        return VariableType.BOOL;
    }

    @Override
    public VariableType visitTernary(GrammarParser.TernaryContext ctx) {

        VariableType exp1 = this.visit(ctx.expression(0));

        if (exp1 != VariableType.BOOL) {
            throw new ParseCancellationException(String.format(TYPE_ERROR_MSG, exp1, "BOOL"));
        }

        Placeholder jmcPlaceholder = writer.writeInstruction(Instruction.JMC, 0);

        VariableType exp2 = this.visit(ctx.expression(1));

        if (exp2 != VariableType.VOID) {
            throw new ParseCancellationException(String.format(TYPE_ERROR_MSG, exp2, "INT, CONSTINT or BOOL"));
        }

        Placeholder jmpPlaceholder = writer.writeInstruction(Instruction.JMP, 0);

        jmcPlaceholder.fill(instructionsWritten);

        VariableType exp3 = this.visit(ctx.expression(2));

        if (exp3 != VariableType.VOID) {
            throw new ParseCancellationException(String.format(TYPE_ERROR_MSG, exp3, "INT, CONSTINT or BOOL"));
        }

        jmpPlaceholder.fill(instructionsWritten);

        return exp2;
    }

}
