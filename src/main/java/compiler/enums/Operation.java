package compiler.enums;

public enum Operation {

    NEG(1), // unarni minus
    ADD(2), // +
    SUB(3), // -
    MUL(4), // *
    DIV(5), // celociselne deleni
    MOD(6), // modulo
    ODD(7), // test lichosti
    EQ(8),  // test rovnosti
    NE(9),  // test nerovnosti
    LT(10), // <
    GE(11), // >=
    GT(12), // >
    LE(13); // <=

    private int code;

    Operation(int code) {
        this.code = code;
    }

    public int code() {
        return this.code;
    }

}
