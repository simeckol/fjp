grammar Grammar;

options {
  language=Java;
}

/******************************************* PARSER RULES **********************************************/

/* RULES */

program
   : functions mainblock
   ;

functions
    : (function)*
    ;

statement
    : declaration SEMICOLON #declarationStatement
    | ifcase #ifcaseStatement
    | switchcases #switchcasesStatement
    | assign SEMICOLON #assignStatement
    | loop #loopStatement
    | expression SEMICOLON #expressionStatement
    | parallelassign SEMICOLON #parallelassignStatement
    ;

declaration
    : type IDENT ASSIGMENT expression
    ;

assign
    : IDENT ASSIGMENT expression
    ;

parallelassign
    : CURLYLEFT (IDENT (COMMA IDENT)*)? CURLYRIGHT ASSIGMENT CURLYLEFT (expression (COMMA expression)*)? CURLYRIGHT
    ;

type
    : opt=(INT | CONSTINT | BOOL | VOID )
    ;

expression
    : PARENTHESLEFT expression PARENTHESRIGHT #parenthesedExpression
    | NEGATION expression #negationExpression
    | expression opt=(LOGAND | LOGOR) expression #logicalExpression
    | expression opt=(EQUALITY | INEQUALITY | GREATER | SMALLER) expression #comparationExpression
    | expression opt=(TIMES | DIVIDE | MODULO) expression #priorityExpression
    | expression opt=(PLUS | MINUS) expression #operationExpression
    | opt=(PLUS | MINUS) expression #signExpression
    | ternary #ternaryExpression
    | funccall #funccallExpression
    | factor #factorExpression
    ;

ifcase
    : IF PARENTHESLEFT expression PARENTHESRIGHT block (ELSE block)?
    ;

funccall
    : IDENT PARENTHESLEFT funcargs PARENTHESRIGHT
    ;
funcargs
    : (expression (COMMA expression)*)?
    ;

ternary
    : PARENTHESLEFT expression QUESTION expression COLON expression PARENTHESRIGHT
    ;

loop
    : whileloop #whileLoopLoop
    | dowhileloop #doWhileLoopLoop
    | repeatuntilloop #repeatUntilLoopLoop
    | forloop #forLoopLoop
    ;

whileloop
    : WHILE PARENTHESLEFT expression PARENTHESRIGHT block
    ;

dowhileloop
    : DO block WHILE PARENTHESLEFT expression PARENTHESRIGHT
    ;

repeatuntilloop
    : REPEAT block UNTIL expression;

forloop
    : FOR PARENTHESLEFT declaration COMMA expression COMMA assign PARENTHESRIGHT block
    ;

block
    : CURLYLEFT (statement)+ CURLYRIGHT
    ;

funcblock
    : CURLYLEFT (statement)* (RETURN expression SEMICOLON)? CURLYRIGHT
    ;

mainblock
    : CURLYLEFT (statement)+ CURLYRIGHT
    ;

function
    : FUNC IDENT PARENTHESLEFT (argument (COMMA argument)*)? PARENTHESRIGHT COLON type funcblock
    ;

argument
    : type IDENT
    ;

factor
    : INTVALUE #intvalueFactor
    | opt=( TRUE | FALSE ) #boolvalueFactor

    | IDENT #identFactor
    ;

switchcases
    : SWITCH PARENTHESLEFT expression PARENTHESRIGHT CURLYLEFT (incase)+ CURLYRIGHT
    ;

incase
    : CASE PARENTHESLEFT expression PARENTHESRIGHT block
    ;

INTVALUE
    : [0-9]+
    ;

/****************************************************** LEXER RULES **********************************************************/

/* FRAGMENTS */

fragment A
     : ('a' | 'A')
     ;

fragment B
     : ('b' | 'B')
     ;

fragment C
     : ('c' | 'C')
     ;

fragment D
     : ('d' | 'D')
     ;

fragment E
     : ('e' | 'E')
     ;

fragment F
     : ('f' | 'F')
     ;

fragment H
     : ('h' | 'H')
     ;

fragment I
     : ('i' | 'I')
     ;

fragment L
     : ('l' | 'L')
     ;

fragment N
     : ('n' | 'N')
     ;

fragment O
     : ('o' | 'O')
     ;

fragment P
     : ('p' | 'P')
     ;

fragment R
     : ('r' | 'R')
     ;

fragment S
     : ('s' | 'S')
     ;

fragment T
     : ('t' | 'T')
     ;

fragment U
     : ('u' | 'U')
     ;

fragment V
     : ('v' | 'V')
     ;

fragment W
     : ('w' | 'W')
     ;

/* KEYWORDS */

BOOL
    : B O O L
    ;

CASE
    : C A S E
    ;

CONSTINT
    : C O N S T I N T
    ;

DO
    : D O
    ;

ELSE
    : E L S E
    ;

FALSE
    : F A L S E
    ;

FOR
    : F O R
    ;

FUNC
    : F U N C
    ;

IF
    : I F
    ;

INT
    : I N T
    ;

REPEAT
    : R E P E A T
    ;

RETURN
    : R E T U R N
    ;

SWITCH
    : S W I T C H
    ;

TRUE
    : T R U E
    ;

UNTIL
    : U N T I L
    ;

VOID
    : V O I D
    ;

WHILE
    : W H I L E
    ;

/* SYMBOLS */

ASSIGMENT
    : '='
    ;

COLON
    : ':'
    ;

COMMA
    : ','
    ;

CURLYLEFT
    : '{'
    ;

CURLYRIGHT
    : '}'
    ;

DIVIDE
    : '/'
    ;

EQUALITY
    : '=='
    ;

INEQUALITY
    : '!='
    ;

GREATER
    : '>'
    ;

LOGAND
    : '&&'
    ;

LOGOR
    : '||'
    ;

MINUS
    : '-'
    ;

MODULO
    : '%'
    ;

NEGATION
    : '!'
    ;

PARENTHESLEFT
    : '('
    ;

PARENTHESRIGHT
    : ')'
    ;

PLUS
    : '+'
    ;

QUESTION
    : '?'
    ;

SEMICOLON
    : ';'
    ;

SMALLER
    : '<'
    ;

TIMES
    : '*'
    ;

/* REGEXES */

IDENT
    : ('a' .. 'z' | 'A' .. 'Z')+
    ;

WHITESYMBOLS
   : [\n\r\t ] -> skip
   ;
