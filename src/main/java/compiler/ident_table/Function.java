package compiler.ident_table;

import compiler.enums.VariableType;

import java.util.LinkedHashMap;

public class Function {

    private int index;
    private VariableType returnType;
    private LinkedHashMap<String, VariableType> args;
    private int length;

    public Function(int index, VariableType returnType, LinkedHashMap<String, VariableType> args) {
        this.index = index;
        this.returnType = returnType;
        this.args = args;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public VariableType getReturnType() {
        return returnType;
    }

    public void setReturnType(VariableType returnType) {
        this.returnType = returnType;
    }

    public LinkedHashMap<String, VariableType> getArgs() {
        return args;
    }

    public void setArgs(LinkedHashMap<String, VariableType> args) {
        this.args = args;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }
}
