package compiler.visitors;

import compiler.enums.Instruction;
import compiler.enums.Operation;
import compiler.enums.VariableType;
import compiler.gen.GrammarBaseVisitor;
import compiler.gen.GrammarParser;
import compiler.ident_table.Function;
import compiler.writer.Placeholder;

import java.util.LinkedHashMap;

import static compiler.Common.*;

public class ProgramVisitor extends GrammarBaseVisitor<Void> {

    @Override
    public Void visitProgram(GrammarParser.ProgramContext ctx) {

        writer.openWriter();

        Placeholder jmpPlaceholder = writer.writeInstruction(Instruction.JMP, 0);
        FunctionVisitor functionVisitor = new FunctionVisitor();

        setupWriteFunction();

        for (GrammarParser.FunctionContext functionCtx : ctx.functions().function()) {
            functionVisitor.visit(functionCtx);
        }

        jmpPlaceholder.fill(instructionsWritten);

        new MainBlockVisitor().visit(ctx.mainblock());

        writer.closeWriter();

        return null;
    }

    private void setupWriteFunction() {
        LinkedHashMap<String, VariableType> args = new LinkedHashMap<>();
        args.put("arg", VariableType.INT);
        Function function = new Function(instructionsWritten, VariableType.VOID, args);
        functionMap.add("write", function);

        Placeholder intPlaceholder = writer.writeInstruction(Instruction.INT, 0);

        // argument
        int argIndex = OFFSET + declarationCount;
        writer.writeInstruction(Instruction.LOD, 1, OFFSET + RETURNOFFSET);
        writer.writeInstruction(Instruction.STO, 0, argIndex);
        declarationCount++;

        // counter
        int counterIndex = OFFSET + declarationCount;
        writer.writeInstruction(Instruction.LIT, 0, 0);
        writer.writeInstruction(Instruction.STO, 0, counterIndex);
        declarationCount++;

        int jmpWhile = instructionsWritten;

        writer.writeInstruction(Instruction.LOD, 0, argIndex);
        writer.writeInstruction(Instruction.LIT, 0, 0);
        writer.writeInstruction(Instruction.OPR, 0, Operation.NE.code());


        Placeholder fisrtjmc = writer.writeInstruction(Instruction.JMC, 0);

        //towrite % 10;
        writer.writeInstruction(Instruction.LOD, 0, argIndex);
        writer.writeInstruction(Instruction.LIT, 0, 10);
        writer.writeInstruction(Instruction.OPR, 0, Operation.MOD.code());

        // towrite = towrite / 10;
        writer.writeInstruction(Instruction.LOD, 0, argIndex);
        writer.writeInstruction(Instruction.LIT, 0, 10);
        writer.writeInstruction(Instruction.OPR, 0, Operation.DIV.code());
        writer.writeInstruction(Instruction.STO, 0, argIndex);

        // counter = counter + 1;
        writer.writeInstruction(Instruction.LOD, 0, counterIndex);
        writer.writeInstruction(Instruction.LIT, 0, 1);
        writer.writeInstruction(Instruction.OPR, 0, Operation.ADD.code());
        writer.writeInstruction(Instruction.STO, 0, counterIndex);

        writer.writeInstruction(Instruction.JMP, 0, jmpWhile);

        fisrtjmc.fill(instructionsWritten);

        // int i = 0;
        int iIndex = OFFSET + declarationCount;
        writer.writeInstruction(Instruction.LIT, 0, 0);
        writer.writeInstruction(Instruction.STO, 0, iIndex);
        declarationCount++;

        int secondJmpIndex = instructionsWritten;

        writer.writeInstruction(Instruction.LOD, 0, iIndex);
        writer.writeInstruction(Instruction.LOD, 0, counterIndex);
        writer.writeInstruction(Instruction.OPR, 0, Operation.LT.code());

        Placeholder secondjmc = writer.writeInstruction(Instruction.JMC, 0);

        writer.writeInstruction(Instruction.LIT, 0, 48);
        writer.writeInstruction(Instruction.OPR, 0, Operation.ADD.code());
        writer.writeInstruction(Instruction.WRI, 0, 0);

        // i = i + 1;
        writer.writeInstruction(Instruction.LOD, 0, iIndex);
        writer.writeInstruction(Instruction.LIT, 0, 1);
        writer.writeInstruction(Instruction.OPR, 0, Operation.ADD.code());
        writer.writeInstruction(Instruction.STO, 0, iIndex);

        writer.writeInstruction(Instruction.JMP, 0, secondJmpIndex);

        secondjmc.fill(instructionsWritten);

        writer.writeInstruction(Instruction.RET, 0, 0);

        intPlaceholder.fill(OFFSET + declarationCount);

    }

}
