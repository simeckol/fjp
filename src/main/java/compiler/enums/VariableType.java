package compiler.enums;

import compiler.gen.GrammarParser;
import org.antlr.v4.runtime.misc.ParseCancellationException;

public enum VariableType {

    INT,
    CONSTINT,
    BOOL,
    VOID;

    public static VariableType getByRuleIndex(int ruleIndex) {
        switch (ruleIndex) {
            case (GrammarParser.INT):
                return VariableType.INT;
            case (GrammarParser.CONSTINT):
                return VariableType.CONSTINT;
            case (GrammarParser.BOOL):
                return VariableType.BOOL;
            case (GrammarParser.VOID):
                return VariableType.VOID;
            default:
                throw new ParseCancellationException("No such variable type.");
        }
    }
}
