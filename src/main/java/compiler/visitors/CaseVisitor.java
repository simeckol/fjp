package compiler.visitors;

import compiler.gen.GrammarBaseVisitor;
import compiler.gen.GrammarParser;

public class CaseVisitor extends GrammarBaseVisitor<Void> {

    @Override
    public Void visitIncase(GrammarParser.IncaseContext ctx) {

        new BlockVisitor().visit(ctx.block());

        return null;

    }

}
