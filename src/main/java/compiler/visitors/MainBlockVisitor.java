package compiler.visitors;

import compiler.enums.Instruction;
import compiler.gen.GrammarBaseVisitor;
import compiler.gen.GrammarParser;
import compiler.writer.Placeholder;

import static compiler.Common.*;

public class MainBlockVisitor extends GrammarBaseVisitor<Void> {

    @Override
    public Void visitMainblock(GrammarParser.MainblockContext ctx) {

        declarationCount = 0;
        currentFuncName = "_main";
        currentFuncArgsCount = 0;

        // skip declared variables
        Placeholder placeholder = writer.writeInstruction(Instruction.INT, 0);

        StatementVisitor statementVisitor = new StatementVisitor();

        for (GrammarParser.StatementContext statementCtx : ctx.statement()) {
            statementVisitor.visit(statementCtx);
        }

        // skip declared variables
        placeholder.fill(OFFSET + maxArgumentsCount + RETURNOFFSET + declarationCount);

        declarationCount = declarationCount + maxArgumentsCount + RETURNOFFSET;

        writer.writeInstruction(Instruction.RET, 0, 0);

        return null;

    }

}
