package compiler.visitors;

import compiler.enums.Instruction;
import compiler.gen.GrammarBaseVisitor;
import compiler.gen.GrammarParser;

import static compiler.Common.*;

public class FuncBlockVisitor extends GrammarBaseVisitor<Void> {

    @Override
    public Void visitFuncblock(GrammarParser.FuncblockContext ctx) {

        StatementVisitor statementVisitor = new StatementVisitor();

        for (GrammarParser.StatementContext statementCtx : ctx.statement()) {
            statementVisitor.visit(statementCtx);
        }

        // return statement
        if (ctx.expression() != null) {
            new ExpressionVisitor().visit(ctx.expression());
            writer.writeInstruction(Instruction.STO, currentLevel, OFFSET);
        }

        writer.writeInstruction(Instruction.RET, 0, 0);

        return null;

    }

}
