package compiler.ident_table;

import java.util.HashMap;

public class VariableMap {

    private static final VariableMap instance = new VariableMap();

    // variable_name - variable map
    private HashMap<String, Variable> variableMap;

    // funcionname - variablemap map
    private HashMap<String, HashMap<String, Variable>> map;

    private VariableMap() {
        map = new HashMap<>();
    }

    public static VariableMap getInstance() {
        return instance;
    }

    public Variable get(String functionName, String variableName) {
        HashMap<String, Variable> varMap = map.get(functionName);
        if (varMap == null) {
            return null;
        }

        return varMap.get(variableName);
    }

    public void add(String functionName, String variableName, Variable var) {
        HashMap<String, Variable> varMap = map.get(functionName);

        if (varMap == null) {
            varMap = new HashMap<>();
            map.put(functionName, varMap);
        }

        varMap.put(variableName, var);
    }

    public boolean contains(String functionName, String variableName) {
        HashMap<String, Variable> varMap = map.get(functionName);
        if (varMap == null) {
            return false;
        }
        return varMap.containsKey(variableName);
    }
}
