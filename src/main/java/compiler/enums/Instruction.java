package compiler.enums;

public enum Instruction {

    LIT, // vlozeni hodnoty na zasobnik
    OPR, // provedeni operace nad vrcholem zasobniku (Operation.java)
    LOD, // ulozeni dane hodnoty na vrchol zasobniku
    STO, // zapsani hodnoty z vrcholu zasobniku do dane promene
    CAL, // zavolani procedury
    RET, // navrat z procedury
    INT, // zvyseni obsahu top-registru zasobniku o danou hodnotu
    JMP, // skok na danou adresu
    JMC, // skok na danou adresu, pokud je hodnota na vrcholu zasobniku 0
    WRI; // write


    @Override
    public String toString() {
        return this.name();
    }
}
