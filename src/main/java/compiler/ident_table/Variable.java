package compiler.ident_table;

import compiler.enums.VariableType;

public class Variable {
    private int level;
    private int declarationIndex;
    private VariableType variableType;

    public Variable(VariableType type, int level, int declarationIndex) {
        this.level = level;
        this.declarationIndex = declarationIndex;
        variableType = type;
    }

    public int getDeclarationIndex() {
        return declarationIndex;
    }

    public VariableType getVariableType() {
        return variableType;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void setDeclarationIndex(int declarationIndex) {
        this.declarationIndex = declarationIndex;
    }
}
