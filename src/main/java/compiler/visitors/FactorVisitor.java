package compiler.visitors;

import compiler.enums.Instruction;
import compiler.enums.VariableType;
import compiler.gen.GrammarBaseVisitor;
import compiler.gen.GrammarParser;
import compiler.ident_table.Variable;
import org.antlr.v4.runtime.misc.ParseCancellationException;

import static compiler.Common.*;

public class FactorVisitor extends GrammarBaseVisitor<VariableType> {

    @Override
    public VariableType visitIntvalueFactor(GrammarParser.IntvalueFactorContext ctx) {
        int value = Integer.parseInt(ctx.INTVALUE().getSymbol().getText());
        writer.writeInstruction(Instruction.LIT, 0, value);

        return VariableType.INT;
    }

    @Override
    public VariableType visitBoolvalueFactor(GrammarParser.BoolvalueFactorContext ctx) {

        int optType = ctx.opt.getType();
        writer.writeInstruction(Instruction.LIT, 0, (optType == GrammarParser.TRUE) ? 1 : 0);

        return VariableType.BOOL;
    }

    @Override
    public VariableType visitIdentFactor(GrammarParser.IdentFactorContext ctx) {
        String varName = ctx.IDENT().getSymbol().getText();
        Variable var = variableMap.get(currentFuncName, varName);
        if (var == null) {
            throw new ParseCancellationException(String.format(VAR_NOT_FOUND_ERROR_MSG, varName, currentFuncName));
        }

        writer.writeInstruction(Instruction.LOD, currentLevel - var.getLevel(), var.getDeclarationIndex());

        return var.getVariableType();
    }

}
