// Generated from C:/Users/user/Documents/Projects/fjp/src/compiler\Grammar.g4 by ANTLR 4.7
package compiler.gen;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class GrammarParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		INTVALUE=1, BOOL=2, CASE=3, CONSTINT=4, DO=5, ELSE=6, FALSE=7, FOR=8, 
		FUNC=9, IF=10, INT=11, REPEAT=12, RETURN=13, SWITCH=14, TRUE=15, UNTIL=16, 
		VOID=17, WHILE=18, ASSIGMENT=19, COLON=20, COMMA=21, CURLYLEFT=22, CURLYRIGHT=23, 
		DIVIDE=24, EQUALITY=25, INEQUALITY=26, GREATER=27, LOGAND=28, LOGOR=29, 
		MINUS=30, MODULO=31, NEGATION=32, PARENTHESLEFT=33, PARENTHESRIGHT=34, 
		PLUS=35, QUESTION=36, SEMICOLON=37, SMALLER=38, TIMES=39, IDENT=40, WHITESYMBOLS=41;
	public static final int
		RULE_program = 0, RULE_functions = 1, RULE_statement = 2, RULE_declaration = 3, 
		RULE_assign = 4, RULE_parallelassign = 5, RULE_type = 6, RULE_expression = 7, 
		RULE_ifcase = 8, RULE_funccall = 9, RULE_funcargs = 10, RULE_ternary = 11, 
		RULE_loop = 12, RULE_whileloop = 13, RULE_dowhileloop = 14, RULE_repeatuntilloop = 15, 
		RULE_forloop = 16, RULE_block = 17, RULE_funcblock = 18, RULE_mainblock = 19, 
		RULE_function = 20, RULE_argument = 21, RULE_factor = 22, RULE_switchcases = 23, 
		RULE_incase = 24;
	public static final String[] ruleNames = {
		"program", "functions", "statement", "declaration", "assign", "parallelassign", 
		"type", "expression", "ifcase", "funccall", "funcargs", "ternary", "loop", 
		"whileloop", "dowhileloop", "repeatuntilloop", "forloop", "block", "funcblock", 
		"mainblock", "function", "argument", "factor", "switchcases", "incase"
	};

	private static final String[] _LITERAL_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, "'='", "':'", "','", "'{'", 
		"'}'", "'/'", "'=='", "'!='", "'>'", "'&&'", "'||'", "'-'", "'%'", "'!'", 
		"'('", "')'", "'+'", "'?'", "';'", "'<'", "'*'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "INTVALUE", "BOOL", "CASE", "CONSTINT", "DO", "ELSE", "FALSE", "FOR", 
		"FUNC", "IF", "INT", "REPEAT", "RETURN", "SWITCH", "TRUE", "UNTIL", "VOID", 
		"WHILE", "ASSIGMENT", "COLON", "COMMA", "CURLYLEFT", "CURLYRIGHT", "DIVIDE", 
		"EQUALITY", "INEQUALITY", "GREATER", "LOGAND", "LOGOR", "MINUS", "MODULO", 
		"NEGATION", "PARENTHESLEFT", "PARENTHESRIGHT", "PLUS", "QUESTION", "SEMICOLON", 
		"SMALLER", "TIMES", "IDENT", "WHITESYMBOLS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Grammar.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public GrammarParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ProgramContext extends ParserRuleContext {
		public FunctionsContext functions() {
			return getRuleContext(FunctionsContext.class,0);
		}
		public MainblockContext mainblock() {
			return getRuleContext(MainblockContext.class,0);
		}
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitProgram(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_program);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(50);
			functions();
			setState(51);
			mainblock();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionsContext extends ParserRuleContext {
		public List<FunctionContext> function() {
			return getRuleContexts(FunctionContext.class);
		}
		public FunctionContext function(int i) {
			return getRuleContext(FunctionContext.class,i);
		}
		public FunctionsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functions; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitFunctions(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionsContext functions() throws RecognitionException {
		FunctionsContext _localctx = new FunctionsContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_functions);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(56);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==FUNC) {
				{
				{
				setState(53);
				function();
				}
				}
				setState(58);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
	 
		public StatementContext() { }
		public void copyFrom(StatementContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class DeclarationStatementContext extends StatementContext {
		public DeclarationContext declaration() {
			return getRuleContext(DeclarationContext.class,0);
		}
		public TerminalNode SEMICOLON() { return getToken(GrammarParser.SEMICOLON, 0); }
		public DeclarationStatementContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitDeclarationStatement(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IfcaseStatementContext extends StatementContext {
		public IfcaseContext ifcase() {
			return getRuleContext(IfcaseContext.class,0);
		}
		public IfcaseStatementContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitIfcaseStatement(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AssignStatementContext extends StatementContext {
		public AssignContext assign() {
			return getRuleContext(AssignContext.class,0);
		}
		public TerminalNode SEMICOLON() { return getToken(GrammarParser.SEMICOLON, 0); }
		public AssignStatementContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitAssignStatement(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SwitchcasesStatementContext extends StatementContext {
		public SwitchcasesContext switchcases() {
			return getRuleContext(SwitchcasesContext.class,0);
		}
		public SwitchcasesStatementContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitSwitchcasesStatement(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LoopStatementContext extends StatementContext {
		public LoopContext loop() {
			return getRuleContext(LoopContext.class,0);
		}
		public LoopStatementContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitLoopStatement(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ExpressionStatementContext extends StatementContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode SEMICOLON() { return getToken(GrammarParser.SEMICOLON, 0); }
		public ExpressionStatementContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitExpressionStatement(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ParallelassignStatementContext extends StatementContext {
		public ParallelassignContext parallelassign() {
			return getRuleContext(ParallelassignContext.class,0);
		}
		public TerminalNode SEMICOLON() { return getToken(GrammarParser.SEMICOLON, 0); }
		public ParallelassignStatementContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitParallelassignStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_statement);
		try {
			setState(74);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
			case 1:
				_localctx = new DeclarationStatementContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(59);
				declaration();
				setState(60);
				match(SEMICOLON);
				}
				break;
			case 2:
				_localctx = new IfcaseStatementContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(62);
				ifcase();
				}
				break;
			case 3:
				_localctx = new SwitchcasesStatementContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(63);
				switchcases();
				}
				break;
			case 4:
				_localctx = new AssignStatementContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(64);
				assign();
				setState(65);
				match(SEMICOLON);
				}
				break;
			case 5:
				_localctx = new LoopStatementContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(67);
				loop();
				}
				break;
			case 6:
				_localctx = new ExpressionStatementContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(68);
				expression(0);
				setState(69);
				match(SEMICOLON);
				}
				break;
			case 7:
				_localctx = new ParallelassignStatementContext(_localctx);
				enterOuterAlt(_localctx, 7);
				{
				setState(71);
				parallelassign();
				setState(72);
				match(SEMICOLON);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclarationContext extends ParserRuleContext {
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode IDENT() { return getToken(GrammarParser.IDENT, 0); }
		public TerminalNode ASSIGMENT() { return getToken(GrammarParser.ASSIGMENT, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public DeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declaration; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitDeclaration(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DeclarationContext declaration() throws RecognitionException {
		DeclarationContext _localctx = new DeclarationContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_declaration);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(76);
			type();
			setState(77);
			match(IDENT);
			setState(78);
			match(ASSIGMENT);
			setState(79);
			expression(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssignContext extends ParserRuleContext {
		public TerminalNode IDENT() { return getToken(GrammarParser.IDENT, 0); }
		public TerminalNode ASSIGMENT() { return getToken(GrammarParser.ASSIGMENT, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public AssignContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assign; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitAssign(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AssignContext assign() throws RecognitionException {
		AssignContext _localctx = new AssignContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_assign);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(81);
			match(IDENT);
			setState(82);
			match(ASSIGMENT);
			setState(83);
			expression(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParallelassignContext extends ParserRuleContext {
		public List<TerminalNode> CURLYLEFT() { return getTokens(GrammarParser.CURLYLEFT); }
		public TerminalNode CURLYLEFT(int i) {
			return getToken(GrammarParser.CURLYLEFT, i);
		}
		public List<TerminalNode> CURLYRIGHT() { return getTokens(GrammarParser.CURLYRIGHT); }
		public TerminalNode CURLYRIGHT(int i) {
			return getToken(GrammarParser.CURLYRIGHT, i);
		}
		public TerminalNode ASSIGMENT() { return getToken(GrammarParser.ASSIGMENT, 0); }
		public List<TerminalNode> IDENT() { return getTokens(GrammarParser.IDENT); }
		public TerminalNode IDENT(int i) {
			return getToken(GrammarParser.IDENT, i);
		}
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(GrammarParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(GrammarParser.COMMA, i);
		}
		public ParallelassignContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parallelassign; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitParallelassign(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParallelassignContext parallelassign() throws RecognitionException {
		ParallelassignContext _localctx = new ParallelassignContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_parallelassign);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(85);
			match(CURLYLEFT);
			setState(94);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==IDENT) {
				{
				setState(86);
				match(IDENT);
				setState(91);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(87);
					match(COMMA);
					setState(88);
					match(IDENT);
					}
					}
					setState(93);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(96);
			match(CURLYRIGHT);
			setState(97);
			match(ASSIGMENT);
			setState(98);
			match(CURLYLEFT);
			setState(107);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << INTVALUE) | (1L << FALSE) | (1L << TRUE) | (1L << MINUS) | (1L << NEGATION) | (1L << PARENTHESLEFT) | (1L << PLUS) | (1L << IDENT))) != 0)) {
				{
				setState(99);
				expression(0);
				setState(104);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(100);
					match(COMMA);
					setState(101);
					expression(0);
					}
					}
					setState(106);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(109);
			match(CURLYRIGHT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeContext extends ParserRuleContext {
		public Token opt;
		public TerminalNode INT() { return getToken(GrammarParser.INT, 0); }
		public TerminalNode CONSTINT() { return getToken(GrammarParser.CONSTINT, 0); }
		public TerminalNode BOOL() { return getToken(GrammarParser.BOOL, 0); }
		public TerminalNode VOID() { return getToken(GrammarParser.VOID, 0); }
		public TypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TypeContext type() throws RecognitionException {
		TypeContext _localctx = new TypeContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_type);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(111);
			((TypeContext)_localctx).opt = _input.LT(1);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BOOL) | (1L << CONSTINT) | (1L << INT) | (1L << VOID))) != 0)) ) {
				((TypeContext)_localctx).opt = (Token)_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
	 
		public ExpressionContext() { }
		public void copyFrom(ExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class NegationExpressionContext extends ExpressionContext {
		public TerminalNode NEGATION() { return getToken(GrammarParser.NEGATION, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public NegationExpressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitNegationExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ComparationExpressionContext extends ExpressionContext {
		public Token opt;
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode EQUALITY() { return getToken(GrammarParser.EQUALITY, 0); }
		public TerminalNode INEQUALITY() { return getToken(GrammarParser.INEQUALITY, 0); }
		public TerminalNode GREATER() { return getToken(GrammarParser.GREATER, 0); }
		public TerminalNode SMALLER() { return getToken(GrammarParser.SMALLER, 0); }
		public ComparationExpressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitComparationExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FunccallExpressionContext extends ExpressionContext {
		public FunccallContext funccall() {
			return getRuleContext(FunccallContext.class,0);
		}
		public FunccallExpressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitFunccallExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FactorExpressionContext extends ExpressionContext {
		public FactorContext factor() {
			return getRuleContext(FactorContext.class,0);
		}
		public FactorExpressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitFactorExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ParenthesedExpressionContext extends ExpressionContext {
		public TerminalNode PARENTHESLEFT() { return getToken(GrammarParser.PARENTHESLEFT, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode PARENTHESRIGHT() { return getToken(GrammarParser.PARENTHESRIGHT, 0); }
		public ParenthesedExpressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitParenthesedExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PriorityExpressionContext extends ExpressionContext {
		public Token opt;
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode TIMES() { return getToken(GrammarParser.TIMES, 0); }
		public TerminalNode DIVIDE() { return getToken(GrammarParser.DIVIDE, 0); }
		public TerminalNode MODULO() { return getToken(GrammarParser.MODULO, 0); }
		public PriorityExpressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitPriorityExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OperationExpressionContext extends ExpressionContext {
		public Token opt;
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode PLUS() { return getToken(GrammarParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(GrammarParser.MINUS, 0); }
		public OperationExpressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitOperationExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TernaryExpressionContext extends ExpressionContext {
		public TernaryContext ternary() {
			return getRuleContext(TernaryContext.class,0);
		}
		public TernaryExpressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitTernaryExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LogicalExpressionContext extends ExpressionContext {
		public Token opt;
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode LOGAND() { return getToken(GrammarParser.LOGAND, 0); }
		public TerminalNode LOGOR() { return getToken(GrammarParser.LOGOR, 0); }
		public LogicalExpressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitLogicalExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SignExpressionContext extends ExpressionContext {
		public Token opt;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode PLUS() { return getToken(GrammarParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(GrammarParser.MINUS, 0); }
		public SignExpressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitSignExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		return expression(0);
	}

	private ExpressionContext expression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExpressionContext _localctx = new ExpressionContext(_ctx, _parentState);
		ExpressionContext _prevctx = _localctx;
		int _startState = 14;
		enterRecursionRule(_localctx, 14, RULE_expression, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(125);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
			case 1:
				{
				_localctx = new ParenthesedExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(114);
				match(PARENTHESLEFT);
				setState(115);
				expression(0);
				setState(116);
				match(PARENTHESRIGHT);
				}
				break;
			case 2:
				{
				_localctx = new NegationExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(118);
				match(NEGATION);
				setState(119);
				expression(9);
				}
				break;
			case 3:
				{
				_localctx = new SignExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(120);
				((SignExpressionContext)_localctx).opt = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==MINUS || _la==PLUS) ) {
					((SignExpressionContext)_localctx).opt = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(121);
				expression(4);
				}
				break;
			case 4:
				{
				_localctx = new TernaryExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(122);
				ternary();
				}
				break;
			case 5:
				{
				_localctx = new FunccallExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(123);
				funccall();
				}
				break;
			case 6:
				{
				_localctx = new FactorExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(124);
				factor();
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(141);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,8,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(139);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,7,_ctx) ) {
					case 1:
						{
						_localctx = new LogicalExpressionContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(127);
						if (!(precpred(_ctx, 8))) throw new FailedPredicateException(this, "precpred(_ctx, 8)");
						setState(128);
						((LogicalExpressionContext)_localctx).opt = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==LOGAND || _la==LOGOR) ) {
							((LogicalExpressionContext)_localctx).opt = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(129);
						expression(9);
						}
						break;
					case 2:
						{
						_localctx = new ComparationExpressionContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(130);
						if (!(precpred(_ctx, 7))) throw new FailedPredicateException(this, "precpred(_ctx, 7)");
						setState(131);
						((ComparationExpressionContext)_localctx).opt = _input.LT(1);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << EQUALITY) | (1L << INEQUALITY) | (1L << GREATER) | (1L << SMALLER))) != 0)) ) {
							((ComparationExpressionContext)_localctx).opt = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(132);
						expression(8);
						}
						break;
					case 3:
						{
						_localctx = new PriorityExpressionContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(133);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(134);
						((PriorityExpressionContext)_localctx).opt = _input.LT(1);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << DIVIDE) | (1L << MODULO) | (1L << TIMES))) != 0)) ) {
							((PriorityExpressionContext)_localctx).opt = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(135);
						expression(7);
						}
						break;
					case 4:
						{
						_localctx = new OperationExpressionContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(136);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(137);
						((OperationExpressionContext)_localctx).opt = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==MINUS || _la==PLUS) ) {
							((OperationExpressionContext)_localctx).opt = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(138);
						expression(6);
						}
						break;
					}
					} 
				}
				setState(143);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,8,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class IfcaseContext extends ParserRuleContext {
		public TerminalNode IF() { return getToken(GrammarParser.IF, 0); }
		public TerminalNode PARENTHESLEFT() { return getToken(GrammarParser.PARENTHESLEFT, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode PARENTHESRIGHT() { return getToken(GrammarParser.PARENTHESRIGHT, 0); }
		public List<BlockContext> block() {
			return getRuleContexts(BlockContext.class);
		}
		public BlockContext block(int i) {
			return getRuleContext(BlockContext.class,i);
		}
		public TerminalNode ELSE() { return getToken(GrammarParser.ELSE, 0); }
		public IfcaseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ifcase; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitIfcase(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IfcaseContext ifcase() throws RecognitionException {
		IfcaseContext _localctx = new IfcaseContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_ifcase);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(144);
			match(IF);
			setState(145);
			match(PARENTHESLEFT);
			setState(146);
			expression(0);
			setState(147);
			match(PARENTHESRIGHT);
			setState(148);
			block();
			setState(151);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ELSE) {
				{
				setState(149);
				match(ELSE);
				setState(150);
				block();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunccallContext extends ParserRuleContext {
		public TerminalNode IDENT() { return getToken(GrammarParser.IDENT, 0); }
		public TerminalNode PARENTHESLEFT() { return getToken(GrammarParser.PARENTHESLEFT, 0); }
		public FuncargsContext funcargs() {
			return getRuleContext(FuncargsContext.class,0);
		}
		public TerminalNode PARENTHESRIGHT() { return getToken(GrammarParser.PARENTHESRIGHT, 0); }
		public FunccallContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_funccall; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitFunccall(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunccallContext funccall() throws RecognitionException {
		FunccallContext _localctx = new FunccallContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_funccall);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(153);
			match(IDENT);
			setState(154);
			match(PARENTHESLEFT);
			setState(155);
			funcargs();
			setState(156);
			match(PARENTHESRIGHT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FuncargsContext extends ParserRuleContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(GrammarParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(GrammarParser.COMMA, i);
		}
		public FuncargsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_funcargs; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitFuncargs(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FuncargsContext funcargs() throws RecognitionException {
		FuncargsContext _localctx = new FuncargsContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_funcargs);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(166);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << INTVALUE) | (1L << FALSE) | (1L << TRUE) | (1L << MINUS) | (1L << NEGATION) | (1L << PARENTHESLEFT) | (1L << PLUS) | (1L << IDENT))) != 0)) {
				{
				setState(158);
				expression(0);
				setState(163);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(159);
					match(COMMA);
					setState(160);
					expression(0);
					}
					}
					setState(165);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TernaryContext extends ParserRuleContext {
		public TerminalNode PARENTHESLEFT() { return getToken(GrammarParser.PARENTHESLEFT, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode QUESTION() { return getToken(GrammarParser.QUESTION, 0); }
		public TerminalNode COLON() { return getToken(GrammarParser.COLON, 0); }
		public TerminalNode PARENTHESRIGHT() { return getToken(GrammarParser.PARENTHESRIGHT, 0); }
		public TernaryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ternary; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitTernary(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TernaryContext ternary() throws RecognitionException {
		TernaryContext _localctx = new TernaryContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_ternary);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(168);
			match(PARENTHESLEFT);
			setState(169);
			expression(0);
			setState(170);
			match(QUESTION);
			setState(171);
			expression(0);
			setState(172);
			match(COLON);
			setState(173);
			expression(0);
			setState(174);
			match(PARENTHESRIGHT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LoopContext extends ParserRuleContext {
		public LoopContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_loop; }
	 
		public LoopContext() { }
		public void copyFrom(LoopContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class RepeatUntilLoopLoopContext extends LoopContext {
		public RepeatuntilloopContext repeatuntilloop() {
			return getRuleContext(RepeatuntilloopContext.class,0);
		}
		public RepeatUntilLoopLoopContext(LoopContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitRepeatUntilLoopLoop(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ForLoopLoopContext extends LoopContext {
		public ForloopContext forloop() {
			return getRuleContext(ForloopContext.class,0);
		}
		public ForLoopLoopContext(LoopContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitForLoopLoop(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class WhileLoopLoopContext extends LoopContext {
		public WhileloopContext whileloop() {
			return getRuleContext(WhileloopContext.class,0);
		}
		public WhileLoopLoopContext(LoopContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitWhileLoopLoop(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class DoWhileLoopLoopContext extends LoopContext {
		public DowhileloopContext dowhileloop() {
			return getRuleContext(DowhileloopContext.class,0);
		}
		public DoWhileLoopLoopContext(LoopContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitDoWhileLoopLoop(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LoopContext loop() throws RecognitionException {
		LoopContext _localctx = new LoopContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_loop);
		try {
			setState(180);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case WHILE:
				_localctx = new WhileLoopLoopContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(176);
				whileloop();
				}
				break;
			case DO:
				_localctx = new DoWhileLoopLoopContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(177);
				dowhileloop();
				}
				break;
			case REPEAT:
				_localctx = new RepeatUntilLoopLoopContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(178);
				repeatuntilloop();
				}
				break;
			case FOR:
				_localctx = new ForLoopLoopContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(179);
				forloop();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WhileloopContext extends ParserRuleContext {
		public TerminalNode WHILE() { return getToken(GrammarParser.WHILE, 0); }
		public TerminalNode PARENTHESLEFT() { return getToken(GrammarParser.PARENTHESLEFT, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode PARENTHESRIGHT() { return getToken(GrammarParser.PARENTHESRIGHT, 0); }
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public WhileloopContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_whileloop; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitWhileloop(this);
			else return visitor.visitChildren(this);
		}
	}

	public final WhileloopContext whileloop() throws RecognitionException {
		WhileloopContext _localctx = new WhileloopContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_whileloop);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(182);
			match(WHILE);
			setState(183);
			match(PARENTHESLEFT);
			setState(184);
			expression(0);
			setState(185);
			match(PARENTHESRIGHT);
			setState(186);
			block();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DowhileloopContext extends ParserRuleContext {
		public TerminalNode DO() { return getToken(GrammarParser.DO, 0); }
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public TerminalNode WHILE() { return getToken(GrammarParser.WHILE, 0); }
		public TerminalNode PARENTHESLEFT() { return getToken(GrammarParser.PARENTHESLEFT, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode PARENTHESRIGHT() { return getToken(GrammarParser.PARENTHESRIGHT, 0); }
		public DowhileloopContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dowhileloop; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitDowhileloop(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DowhileloopContext dowhileloop() throws RecognitionException {
		DowhileloopContext _localctx = new DowhileloopContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_dowhileloop);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(188);
			match(DO);
			setState(189);
			block();
			setState(190);
			match(WHILE);
			setState(191);
			match(PARENTHESLEFT);
			setState(192);
			expression(0);
			setState(193);
			match(PARENTHESRIGHT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RepeatuntilloopContext extends ParserRuleContext {
		public TerminalNode REPEAT() { return getToken(GrammarParser.REPEAT, 0); }
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public TerminalNode UNTIL() { return getToken(GrammarParser.UNTIL, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public RepeatuntilloopContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_repeatuntilloop; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitRepeatuntilloop(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RepeatuntilloopContext repeatuntilloop() throws RecognitionException {
		RepeatuntilloopContext _localctx = new RepeatuntilloopContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_repeatuntilloop);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(195);
			match(REPEAT);
			setState(196);
			block();
			setState(197);
			match(UNTIL);
			setState(198);
			expression(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ForloopContext extends ParserRuleContext {
		public TerminalNode FOR() { return getToken(GrammarParser.FOR, 0); }
		public TerminalNode PARENTHESLEFT() { return getToken(GrammarParser.PARENTHESLEFT, 0); }
		public DeclarationContext declaration() {
			return getRuleContext(DeclarationContext.class,0);
		}
		public List<TerminalNode> COMMA() { return getTokens(GrammarParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(GrammarParser.COMMA, i);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public AssignContext assign() {
			return getRuleContext(AssignContext.class,0);
		}
		public TerminalNode PARENTHESRIGHT() { return getToken(GrammarParser.PARENTHESRIGHT, 0); }
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public ForloopContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_forloop; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitForloop(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ForloopContext forloop() throws RecognitionException {
		ForloopContext _localctx = new ForloopContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_forloop);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(200);
			match(FOR);
			setState(201);
			match(PARENTHESLEFT);
			setState(202);
			declaration();
			setState(203);
			match(COMMA);
			setState(204);
			expression(0);
			setState(205);
			match(COMMA);
			setState(206);
			assign();
			setState(207);
			match(PARENTHESRIGHT);
			setState(208);
			block();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlockContext extends ParserRuleContext {
		public TerminalNode CURLYLEFT() { return getToken(GrammarParser.CURLYLEFT, 0); }
		public TerminalNode CURLYRIGHT() { return getToken(GrammarParser.CURLYRIGHT, 0); }
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public BlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_block; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitBlock(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BlockContext block() throws RecognitionException {
		BlockContext _localctx = new BlockContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_block);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(210);
			match(CURLYLEFT);
			setState(212); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(211);
				statement();
				}
				}
				setState(214); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << INTVALUE) | (1L << BOOL) | (1L << CONSTINT) | (1L << DO) | (1L << FALSE) | (1L << FOR) | (1L << IF) | (1L << INT) | (1L << REPEAT) | (1L << SWITCH) | (1L << TRUE) | (1L << VOID) | (1L << WHILE) | (1L << CURLYLEFT) | (1L << MINUS) | (1L << NEGATION) | (1L << PARENTHESLEFT) | (1L << PLUS) | (1L << IDENT))) != 0) );
			setState(216);
			match(CURLYRIGHT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FuncblockContext extends ParserRuleContext {
		public TerminalNode CURLYLEFT() { return getToken(GrammarParser.CURLYLEFT, 0); }
		public TerminalNode CURLYRIGHT() { return getToken(GrammarParser.CURLYRIGHT, 0); }
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public TerminalNode RETURN() { return getToken(GrammarParser.RETURN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode SEMICOLON() { return getToken(GrammarParser.SEMICOLON, 0); }
		public FuncblockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_funcblock; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitFuncblock(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FuncblockContext funcblock() throws RecognitionException {
		FuncblockContext _localctx = new FuncblockContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_funcblock);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(218);
			match(CURLYLEFT);
			setState(222);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << INTVALUE) | (1L << BOOL) | (1L << CONSTINT) | (1L << DO) | (1L << FALSE) | (1L << FOR) | (1L << IF) | (1L << INT) | (1L << REPEAT) | (1L << SWITCH) | (1L << TRUE) | (1L << VOID) | (1L << WHILE) | (1L << CURLYLEFT) | (1L << MINUS) | (1L << NEGATION) | (1L << PARENTHESLEFT) | (1L << PLUS) | (1L << IDENT))) != 0)) {
				{
				{
				setState(219);
				statement();
				}
				}
				setState(224);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(229);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==RETURN) {
				{
				setState(225);
				match(RETURN);
				setState(226);
				expression(0);
				setState(227);
				match(SEMICOLON);
				}
			}

			setState(231);
			match(CURLYRIGHT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MainblockContext extends ParserRuleContext {
		public TerminalNode CURLYLEFT() { return getToken(GrammarParser.CURLYLEFT, 0); }
		public TerminalNode CURLYRIGHT() { return getToken(GrammarParser.CURLYRIGHT, 0); }
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public MainblockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_mainblock; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitMainblock(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MainblockContext mainblock() throws RecognitionException {
		MainblockContext _localctx = new MainblockContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_mainblock);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(233);
			match(CURLYLEFT);
			setState(235); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(234);
				statement();
				}
				}
				setState(237); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << INTVALUE) | (1L << BOOL) | (1L << CONSTINT) | (1L << DO) | (1L << FALSE) | (1L << FOR) | (1L << IF) | (1L << INT) | (1L << REPEAT) | (1L << SWITCH) | (1L << TRUE) | (1L << VOID) | (1L << WHILE) | (1L << CURLYLEFT) | (1L << MINUS) | (1L << NEGATION) | (1L << PARENTHESLEFT) | (1L << PLUS) | (1L << IDENT))) != 0) );
			setState(239);
			match(CURLYRIGHT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionContext extends ParserRuleContext {
		public TerminalNode FUNC() { return getToken(GrammarParser.FUNC, 0); }
		public TerminalNode IDENT() { return getToken(GrammarParser.IDENT, 0); }
		public TerminalNode PARENTHESLEFT() { return getToken(GrammarParser.PARENTHESLEFT, 0); }
		public TerminalNode PARENTHESRIGHT() { return getToken(GrammarParser.PARENTHESRIGHT, 0); }
		public TerminalNode COLON() { return getToken(GrammarParser.COLON, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public FuncblockContext funcblock() {
			return getRuleContext(FuncblockContext.class,0);
		}
		public List<ArgumentContext> argument() {
			return getRuleContexts(ArgumentContext.class);
		}
		public ArgumentContext argument(int i) {
			return getRuleContext(ArgumentContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(GrammarParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(GrammarParser.COMMA, i);
		}
		public FunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionContext function() throws RecognitionException {
		FunctionContext _localctx = new FunctionContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_function);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(241);
			match(FUNC);
			setState(242);
			match(IDENT);
			setState(243);
			match(PARENTHESLEFT);
			setState(252);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BOOL) | (1L << CONSTINT) | (1L << INT) | (1L << VOID))) != 0)) {
				{
				setState(244);
				argument();
				setState(249);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(245);
					match(COMMA);
					setState(246);
					argument();
					}
					}
					setState(251);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(254);
			match(PARENTHESRIGHT);
			setState(255);
			match(COLON);
			setState(256);
			type();
			setState(257);
			funcblock();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArgumentContext extends ParserRuleContext {
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode IDENT() { return getToken(GrammarParser.IDENT, 0); }
		public ArgumentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_argument; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitArgument(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ArgumentContext argument() throws RecognitionException {
		ArgumentContext _localctx = new ArgumentContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_argument);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(259);
			type();
			setState(260);
			match(IDENT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FactorContext extends ParserRuleContext {
		public FactorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_factor; }
	 
		public FactorContext() { }
		public void copyFrom(FactorContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class IdentFactorContext extends FactorContext {
		public TerminalNode IDENT() { return getToken(GrammarParser.IDENT, 0); }
		public IdentFactorContext(FactorContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitIdentFactor(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IntvalueFactorContext extends FactorContext {
		public TerminalNode INTVALUE() { return getToken(GrammarParser.INTVALUE, 0); }
		public IntvalueFactorContext(FactorContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitIntvalueFactor(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BoolvalueFactorContext extends FactorContext {
		public Token opt;
		public TerminalNode TRUE() { return getToken(GrammarParser.TRUE, 0); }
		public TerminalNode FALSE() { return getToken(GrammarParser.FALSE, 0); }
		public BoolvalueFactorContext(FactorContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitBoolvalueFactor(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FactorContext factor() throws RecognitionException {
		FactorContext _localctx = new FactorContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_factor);
		int _la;
		try {
			setState(265);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case INTVALUE:
				_localctx = new IntvalueFactorContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(262);
				match(INTVALUE);
				}
				break;
			case FALSE:
			case TRUE:
				_localctx = new BoolvalueFactorContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(263);
				((BoolvalueFactorContext)_localctx).opt = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==FALSE || _la==TRUE) ) {
					((BoolvalueFactorContext)_localctx).opt = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case IDENT:
				_localctx = new IdentFactorContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(264);
				match(IDENT);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SwitchcasesContext extends ParserRuleContext {
		public TerminalNode SWITCH() { return getToken(GrammarParser.SWITCH, 0); }
		public TerminalNode PARENTHESLEFT() { return getToken(GrammarParser.PARENTHESLEFT, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode PARENTHESRIGHT() { return getToken(GrammarParser.PARENTHESRIGHT, 0); }
		public TerminalNode CURLYLEFT() { return getToken(GrammarParser.CURLYLEFT, 0); }
		public TerminalNode CURLYRIGHT() { return getToken(GrammarParser.CURLYRIGHT, 0); }
		public List<IncaseContext> incase() {
			return getRuleContexts(IncaseContext.class);
		}
		public IncaseContext incase(int i) {
			return getRuleContext(IncaseContext.class,i);
		}
		public SwitchcasesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_switchcases; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitSwitchcases(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SwitchcasesContext switchcases() throws RecognitionException {
		SwitchcasesContext _localctx = new SwitchcasesContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_switchcases);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(267);
			match(SWITCH);
			setState(268);
			match(PARENTHESLEFT);
			setState(269);
			expression(0);
			setState(270);
			match(PARENTHESRIGHT);
			setState(271);
			match(CURLYLEFT);
			setState(273); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(272);
				incase();
				}
				}
				setState(275); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==CASE );
			setState(277);
			match(CURLYRIGHT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IncaseContext extends ParserRuleContext {
		public TerminalNode CASE() { return getToken(GrammarParser.CASE, 0); }
		public TerminalNode PARENTHESLEFT() { return getToken(GrammarParser.PARENTHESLEFT, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode PARENTHESRIGHT() { return getToken(GrammarParser.PARENTHESRIGHT, 0); }
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public IncaseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_incase; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitIncase(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IncaseContext incase() throws RecognitionException {
		IncaseContext _localctx = new IncaseContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_incase);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(279);
			match(CASE);
			setState(280);
			match(PARENTHESLEFT);
			setState(281);
			expression(0);
			setState(282);
			match(PARENTHESRIGHT);
			setState(283);
			block();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 7:
			return expression_sempred((ExpressionContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean expression_sempred(ExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 8);
		case 1:
			return precpred(_ctx, 7);
		case 2:
			return precpred(_ctx, 6);
		case 3:
			return precpred(_ctx, 5);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3+\u0120\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\3\2\3\2\3\2\3\3\7\39\n\3\f\3\16\3<\13\3\3\4\3\4\3\4\3\4\3\4"+
		"\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\5\4M\n\4\3\5\3\5\3\5\3\5\3\5"+
		"\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\7\7\\\n\7\f\7\16\7_\13\7\5\7a\n\7\3\7"+
		"\3\7\3\7\3\7\3\7\3\7\7\7i\n\7\f\7\16\7l\13\7\5\7n\n\7\3\7\3\7\3\b\3\b"+
		"\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\5\t\u0080\n\t\3\t\3\t"+
		"\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\7\t\u008e\n\t\f\t\16\t\u0091"+
		"\13\t\3\n\3\n\3\n\3\n\3\n\3\n\3\n\5\n\u009a\n\n\3\13\3\13\3\13\3\13\3"+
		"\13\3\f\3\f\3\f\7\f\u00a4\n\f\f\f\16\f\u00a7\13\f\5\f\u00a9\n\f\3\r\3"+
		"\r\3\r\3\r\3\r\3\r\3\r\3\r\3\16\3\16\3\16\3\16\5\16\u00b7\n\16\3\17\3"+
		"\17\3\17\3\17\3\17\3\17\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\21\3\21\3"+
		"\21\3\21\3\21\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\23\3"+
		"\23\6\23\u00d7\n\23\r\23\16\23\u00d8\3\23\3\23\3\24\3\24\7\24\u00df\n"+
		"\24\f\24\16\24\u00e2\13\24\3\24\3\24\3\24\3\24\5\24\u00e8\n\24\3\24\3"+
		"\24\3\25\3\25\6\25\u00ee\n\25\r\25\16\25\u00ef\3\25\3\25\3\26\3\26\3\26"+
		"\3\26\3\26\3\26\7\26\u00fa\n\26\f\26\16\26\u00fd\13\26\5\26\u00ff\n\26"+
		"\3\26\3\26\3\26\3\26\3\26\3\27\3\27\3\27\3\30\3\30\3\30\5\30\u010c\n\30"+
		"\3\31\3\31\3\31\3\31\3\31\3\31\6\31\u0114\n\31\r\31\16\31\u0115\3\31\3"+
		"\31\3\32\3\32\3\32\3\32\3\32\3\32\3\32\2\3\20\33\2\4\6\b\n\f\16\20\22"+
		"\24\26\30\32\34\36 \"$&(*,.\60\62\2\b\6\2\4\4\6\6\r\r\23\23\4\2  %%\3"+
		"\2\36\37\4\2\33\35((\5\2\32\32!!))\4\2\t\t\21\21\2\u0129\2\64\3\2\2\2"+
		"\4:\3\2\2\2\6L\3\2\2\2\bN\3\2\2\2\nS\3\2\2\2\fW\3\2\2\2\16q\3\2\2\2\20"+
		"\177\3\2\2\2\22\u0092\3\2\2\2\24\u009b\3\2\2\2\26\u00a8\3\2\2\2\30\u00aa"+
		"\3\2\2\2\32\u00b6\3\2\2\2\34\u00b8\3\2\2\2\36\u00be\3\2\2\2 \u00c5\3\2"+
		"\2\2\"\u00ca\3\2\2\2$\u00d4\3\2\2\2&\u00dc\3\2\2\2(\u00eb\3\2\2\2*\u00f3"+
		"\3\2\2\2,\u0105\3\2\2\2.\u010b\3\2\2\2\60\u010d\3\2\2\2\62\u0119\3\2\2"+
		"\2\64\65\5\4\3\2\65\66\5(\25\2\66\3\3\2\2\2\679\5*\26\28\67\3\2\2\29<"+
		"\3\2\2\2:8\3\2\2\2:;\3\2\2\2;\5\3\2\2\2<:\3\2\2\2=>\5\b\5\2>?\7\'\2\2"+
		"?M\3\2\2\2@M\5\22\n\2AM\5\60\31\2BC\5\n\6\2CD\7\'\2\2DM\3\2\2\2EM\5\32"+
		"\16\2FG\5\20\t\2GH\7\'\2\2HM\3\2\2\2IJ\5\f\7\2JK\7\'\2\2KM\3\2\2\2L=\3"+
		"\2\2\2L@\3\2\2\2LA\3\2\2\2LB\3\2\2\2LE\3\2\2\2LF\3\2\2\2LI\3\2\2\2M\7"+
		"\3\2\2\2NO\5\16\b\2OP\7*\2\2PQ\7\25\2\2QR\5\20\t\2R\t\3\2\2\2ST\7*\2\2"+
		"TU\7\25\2\2UV\5\20\t\2V\13\3\2\2\2W`\7\30\2\2X]\7*\2\2YZ\7\27\2\2Z\\\7"+
		"*\2\2[Y\3\2\2\2\\_\3\2\2\2][\3\2\2\2]^\3\2\2\2^a\3\2\2\2_]\3\2\2\2`X\3"+
		"\2\2\2`a\3\2\2\2ab\3\2\2\2bc\7\31\2\2cd\7\25\2\2dm\7\30\2\2ej\5\20\t\2"+
		"fg\7\27\2\2gi\5\20\t\2hf\3\2\2\2il\3\2\2\2jh\3\2\2\2jk\3\2\2\2kn\3\2\2"+
		"\2lj\3\2\2\2me\3\2\2\2mn\3\2\2\2no\3\2\2\2op\7\31\2\2p\r\3\2\2\2qr\t\2"+
		"\2\2r\17\3\2\2\2st\b\t\1\2tu\7#\2\2uv\5\20\t\2vw\7$\2\2w\u0080\3\2\2\2"+
		"xy\7\"\2\2y\u0080\5\20\t\13z{\t\3\2\2{\u0080\5\20\t\6|\u0080\5\30\r\2"+
		"}\u0080\5\24\13\2~\u0080\5.\30\2\177s\3\2\2\2\177x\3\2\2\2\177z\3\2\2"+
		"\2\177|\3\2\2\2\177}\3\2\2\2\177~\3\2\2\2\u0080\u008f\3\2\2\2\u0081\u0082"+
		"\f\n\2\2\u0082\u0083\t\4\2\2\u0083\u008e\5\20\t\13\u0084\u0085\f\t\2\2"+
		"\u0085\u0086\t\5\2\2\u0086\u008e\5\20\t\n\u0087\u0088\f\b\2\2\u0088\u0089"+
		"\t\6\2\2\u0089\u008e\5\20\t\t\u008a\u008b\f\7\2\2\u008b\u008c\t\3\2\2"+
		"\u008c\u008e\5\20\t\b\u008d\u0081\3\2\2\2\u008d\u0084\3\2\2\2\u008d\u0087"+
		"\3\2\2\2\u008d\u008a\3\2\2\2\u008e\u0091\3\2\2\2\u008f\u008d\3\2\2\2\u008f"+
		"\u0090\3\2\2\2\u0090\21\3\2\2\2\u0091\u008f\3\2\2\2\u0092\u0093\7\f\2"+
		"\2\u0093\u0094\7#\2\2\u0094\u0095\5\20\t\2\u0095\u0096\7$\2\2\u0096\u0099"+
		"\5$\23\2\u0097\u0098\7\b\2\2\u0098\u009a\5$\23\2\u0099\u0097\3\2\2\2\u0099"+
		"\u009a\3\2\2\2\u009a\23\3\2\2\2\u009b\u009c\7*\2\2\u009c\u009d\7#\2\2"+
		"\u009d\u009e\5\26\f\2\u009e\u009f\7$\2\2\u009f\25\3\2\2\2\u00a0\u00a5"+
		"\5\20\t\2\u00a1\u00a2\7\27\2\2\u00a2\u00a4\5\20\t\2\u00a3\u00a1\3\2\2"+
		"\2\u00a4\u00a7\3\2\2\2\u00a5\u00a3\3\2\2\2\u00a5\u00a6\3\2\2\2\u00a6\u00a9"+
		"\3\2\2\2\u00a7\u00a5\3\2\2\2\u00a8\u00a0\3\2\2\2\u00a8\u00a9\3\2\2\2\u00a9"+
		"\27\3\2\2\2\u00aa\u00ab\7#\2\2\u00ab\u00ac\5\20\t\2\u00ac\u00ad\7&\2\2"+
		"\u00ad\u00ae\5\20\t\2\u00ae\u00af\7\26\2\2\u00af\u00b0\5\20\t\2\u00b0"+
		"\u00b1\7$\2\2\u00b1\31\3\2\2\2\u00b2\u00b7\5\34\17\2\u00b3\u00b7\5\36"+
		"\20\2\u00b4\u00b7\5 \21\2\u00b5\u00b7\5\"\22\2\u00b6\u00b2\3\2\2\2\u00b6"+
		"\u00b3\3\2\2\2\u00b6\u00b4\3\2\2\2\u00b6\u00b5\3\2\2\2\u00b7\33\3\2\2"+
		"\2\u00b8\u00b9\7\24\2\2\u00b9\u00ba\7#\2\2\u00ba\u00bb\5\20\t\2\u00bb"+
		"\u00bc\7$\2\2\u00bc\u00bd\5$\23\2\u00bd\35\3\2\2\2\u00be\u00bf\7\7\2\2"+
		"\u00bf\u00c0\5$\23\2\u00c0\u00c1\7\24\2\2\u00c1\u00c2\7#\2\2\u00c2\u00c3"+
		"\5\20\t\2\u00c3\u00c4\7$\2\2\u00c4\37\3\2\2\2\u00c5\u00c6\7\16\2\2\u00c6"+
		"\u00c7\5$\23\2\u00c7\u00c8\7\22\2\2\u00c8\u00c9\5\20\t\2\u00c9!\3\2\2"+
		"\2\u00ca\u00cb\7\n\2\2\u00cb\u00cc\7#\2\2\u00cc\u00cd\5\b\5\2\u00cd\u00ce"+
		"\7\27\2\2\u00ce\u00cf\5\20\t\2\u00cf\u00d0\7\27\2\2\u00d0\u00d1\5\n\6"+
		"\2\u00d1\u00d2\7$\2\2\u00d2\u00d3\5$\23\2\u00d3#\3\2\2\2\u00d4\u00d6\7"+
		"\30\2\2\u00d5\u00d7\5\6\4\2\u00d6\u00d5\3\2\2\2\u00d7\u00d8\3\2\2\2\u00d8"+
		"\u00d6\3\2\2\2\u00d8\u00d9\3\2\2\2\u00d9\u00da\3\2\2\2\u00da\u00db\7\31"+
		"\2\2\u00db%\3\2\2\2\u00dc\u00e0\7\30\2\2\u00dd\u00df\5\6\4\2\u00de\u00dd"+
		"\3\2\2\2\u00df\u00e2\3\2\2\2\u00e0\u00de\3\2\2\2\u00e0\u00e1\3\2\2\2\u00e1"+
		"\u00e7\3\2\2\2\u00e2\u00e0\3\2\2\2\u00e3\u00e4\7\17\2\2\u00e4\u00e5\5"+
		"\20\t\2\u00e5\u00e6\7\'\2\2\u00e6\u00e8\3\2\2\2\u00e7\u00e3\3\2\2\2\u00e7"+
		"\u00e8\3\2\2\2\u00e8\u00e9\3\2\2\2\u00e9\u00ea\7\31\2\2\u00ea\'\3\2\2"+
		"\2\u00eb\u00ed\7\30\2\2\u00ec\u00ee\5\6\4\2\u00ed\u00ec\3\2\2\2\u00ee"+
		"\u00ef\3\2\2\2\u00ef\u00ed\3\2\2\2\u00ef\u00f0\3\2\2\2\u00f0\u00f1\3\2"+
		"\2\2\u00f1\u00f2\7\31\2\2\u00f2)\3\2\2\2\u00f3\u00f4\7\13\2\2\u00f4\u00f5"+
		"\7*\2\2\u00f5\u00fe\7#\2\2\u00f6\u00fb\5,\27\2\u00f7\u00f8\7\27\2\2\u00f8"+
		"\u00fa\5,\27\2\u00f9\u00f7\3\2\2\2\u00fa\u00fd\3\2\2\2\u00fb\u00f9\3\2"+
		"\2\2\u00fb\u00fc\3\2\2\2\u00fc\u00ff\3\2\2\2\u00fd\u00fb\3\2\2\2\u00fe"+
		"\u00f6\3\2\2\2\u00fe\u00ff\3\2\2\2\u00ff\u0100\3\2\2\2\u0100\u0101\7$"+
		"\2\2\u0101\u0102\7\26\2\2\u0102\u0103\5\16\b\2\u0103\u0104\5&\24\2\u0104"+
		"+\3\2\2\2\u0105\u0106\5\16\b\2\u0106\u0107\7*\2\2\u0107-\3\2\2\2\u0108"+
		"\u010c\7\3\2\2\u0109\u010c\t\7\2\2\u010a\u010c\7*\2\2\u010b\u0108\3\2"+
		"\2\2\u010b\u0109\3\2\2\2\u010b\u010a\3\2\2\2\u010c/\3\2\2\2\u010d\u010e"+
		"\7\20\2\2\u010e\u010f\7#\2\2\u010f\u0110\5\20\t\2\u0110\u0111\7$\2\2\u0111"+
		"\u0113\7\30\2\2\u0112\u0114\5\62\32\2\u0113\u0112\3\2\2\2\u0114\u0115"+
		"\3\2\2\2\u0115\u0113\3\2\2\2\u0115\u0116\3\2\2\2\u0116\u0117\3\2\2\2\u0117"+
		"\u0118\7\31\2\2\u0118\61\3\2\2\2\u0119\u011a\7\5\2\2\u011a\u011b\7#\2"+
		"\2\u011b\u011c\5\20\t\2\u011c\u011d\7$\2\2\u011d\u011e\5$\23\2\u011e\63"+
		"\3\2\2\2\27:L]`jm\177\u008d\u008f\u0099\u00a5\u00a8\u00b6\u00d8\u00e0"+
		"\u00e7\u00ef\u00fb\u00fe\u010b\u0115";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}