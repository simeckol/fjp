func fact(int n) : int
{
    int ret = 1;
    if(n == 1){
        ret =  1;
    } else {
        ret =  n * (fact(n-1));
    }
    return ret;
}

{
  write(fact(5));
}
