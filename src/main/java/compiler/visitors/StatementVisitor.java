package compiler.visitors;

import compiler.enums.Instruction;
import compiler.enums.Operation;
import compiler.enums.VariableType;
import compiler.gen.GrammarBaseVisitor;
import compiler.gen.GrammarParser;
import compiler.ident_table.Variable;
import compiler.writer.Placeholder;
import org.antlr.v4.runtime.misc.ParseCancellationException;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.ArrayList;
import java.util.List;

import static compiler.Common.*;

public class StatementVisitor extends GrammarBaseVisitor<Void> {

    @Override
    public Void visitDeclarationStatement(GrammarParser.DeclarationStatementContext ctx) {
        return this.visit(ctx.declaration());
    }

    @Override
    public Void visitDeclaration(GrammarParser.DeclarationContext ctx) {

        VariableType variableType = VariableType.getByRuleIndex(ctx.type().opt.getType());

        if (variableType == VariableType.VOID) {
            throw new ParseCancellationException(String.format(TYPE_ERROR_MSG, variableType, "INT, CONSTINT or BOOL"));
        }

        String variableName = ctx.IDENT().getSymbol().getText();

        if (!variableMap.contains(currentFuncName, variableName)) {

            VariableType type = new ExpressionVisitor().visit(ctx.expression());

            if (type == VariableType.VOID) {
                throw new ParseCancellationException(String.format(TYPE_ERROR_MSG, type, "INT, CONSTINT or BOOL"));
            }

            VariableType varType = VariableType.getByRuleIndex(ctx.type().opt.getType());
            Variable newVar = new Variable(varType, currentLevel, OFFSET + currentFuncArgsCount + declarationCount);

            if (!((varType == VariableType.CONSTINT || varType == VariableType.INT) && (type == VariableType.INT)) && varType != type) {
                throw new ParseCancellationException(String.format("Variable %s type does not match with type of assigned expression.", variableName));
            }

            variableMap.add(currentFuncName, variableName, newVar);
            writer.writeInstruction(Instruction.STO, 0, newVar.getDeclarationIndex());

            // increment declaration count
            declarationCount++;


        } else {
            // variable already exists
            throw new ParseCancellationException(String.format("Variable %s already exists", variableName));
        }


        return null;
    }

    @Override
    public Void visitIfcaseStatement(GrammarParser.IfcaseStatementContext ctx) {

        GrammarParser.IfcaseContext ifcase = ctx.ifcase();

        ExpressionVisitor expressionVisitor = new ExpressionVisitor();
        BlockVisitor blockVisitor = new BlockVisitor();

        VariableType ifexp = expressionVisitor.visit(ifcase.expression());

        if (ifexp != VariableType.BOOL) {
            throw new ParseCancellationException(String.format(TYPE_ERROR_MSG, ifexp, "BOOL"));
        }

        Placeholder jmcPlaceholder = writer.writeInstruction(Instruction.JMC, 0);

        blockVisitor.visit(ifcase.block(0));

        jmcPlaceholder.fill(instructionsWritten);

        if (ctx.ifcase().block(1) != null) {

            // skok pres else
            Placeholder jmpPlaceholder = writer.writeInstruction(Instruction.JMP, 0);

            // skok na else
            jmcPlaceholder.fill(instructionsWritten);

            blockVisitor.visit(ifcase.block(1));

            jmpPlaceholder.fill(instructionsWritten);

        }

        return null;
    }

    @Override
    public Void visitExpressionStatement(GrammarParser.ExpressionStatementContext ctx) {

        new ExpressionVisitor().visit(ctx.expression());

        return null;
    }

    @Override
    public Void visitWhileLoopLoop(GrammarParser.WhileLoopLoopContext ctx) {

        GrammarParser.WhileloopContext whileloop = ctx.whileloop();

        int firstIndex = instructionsWritten;

        VariableType type = new ExpressionVisitor().visit(whileloop.expression());
        if (type != VariableType.BOOL) {
            throw new ParseCancellationException(CONDITION_ERROR_MSG);
        }

        // jump na konec pokud false
        Placeholder jmcPlaceholder = writer.writeInstruction(Instruction.JMC, 0);

        new BlockVisitor().visit(whileloop.block());

        // back to start
        writer.writeInstruction(Instruction.JMP, 0, firstIndex);

        jmcPlaceholder.fill(instructionsWritten);

        return null;
    }

    @Override
    public Void visitDoWhileLoopLoop(GrammarParser.DoWhileLoopLoopContext ctx) {
        GrammarParser.DowhileloopContext dowhileloop = ctx.dowhileloop();

        int firstIndex = instructionsWritten;

        new BlockVisitor().visit(dowhileloop.block());

        VariableType type = new ExpressionVisitor().visit(dowhileloop.expression());
        if (type != VariableType.BOOL) {
            throw new ParseCancellationException(CONDITION_ERROR_MSG);
        }

        Placeholder jmcPlaceholder = writer.writeInstruction(Instruction.JMC, 0);

        writer.writeInstruction(Instruction.JMP, 0, firstIndex);

        jmcPlaceholder.fill(instructionsWritten);

        return null;
    }


    @Override
    public Void visitRepeatUntilLoopLoop(GrammarParser.RepeatUntilLoopLoopContext ctx) {
        GrammarParser.RepeatuntilloopContext repeatUntilCtx = ctx.repeatuntilloop();

        int firstIndex = instructionsWritten;

        new BlockVisitor().visit(repeatUntilCtx.block());
        VariableType type = new ExpressionVisitor().visit(repeatUntilCtx.expression());
        if (type != VariableType.BOOL) {
            throw new ParseCancellationException(CONDITION_ERROR_MSG);
        }

        writer.writeInstruction(Instruction.JMC, 0, firstIndex);

        return null;
    }

    @Override
    public Void visitForLoopLoop(GrammarParser.ForLoopLoopContext ctx) {

        GrammarParser.ForloopContext forloop = ctx.forloop();

        this.visit(forloop.declaration());

        int firstInstruction = instructionsWritten;

        VariableType type = new ExpressionVisitor().visit(forloop.expression());
        if (type != VariableType.BOOL) {
            throw new ParseCancellationException(CONDITION_ERROR_MSG);
        }

        Placeholder jmcPlaceholder = writer.writeInstruction(Instruction.JMC, 0);

        this.visit(forloop.assign());
        new BlockVisitor().visit(forloop.block());

        writer.writeInstruction(Instruction.JMP, 0, firstInstruction);

        jmcPlaceholder.fill(instructionsWritten);

        return null;
    }

    @Override
    public Void visitSwitchcasesStatement(GrammarParser.SwitchcasesStatementContext ctx) {

        GrammarParser.SwitchcasesContext switchcases = ctx.switchcases();

        ExpressionVisitor expressionVisitor = new ExpressionVisitor();

        List<Placeholder> endJumps = new ArrayList<>();

        for (GrammarParser.IncaseContext incaseContext : switchcases.incase()) {

            VariableType switchedType = expressionVisitor.visit(switchcases.expression());
            VariableType caseType = expressionVisitor.visit(incaseContext.expression());

            if (switchedType != VariableType.INT && switchedType != VariableType.CONSTINT && caseType != VariableType.INT && caseType != VariableType.CONSTINT) {
                throw new ParseCancellationException(String.format(TYPE_ERROR_MSG, switchedType, "INT or CONSTINT"));
            }

            writer.writeInstruction(Instruction.OPR, 0, Operation.EQ.code());

            Placeholder jmcPlaceholder = writer.writeInstruction(Instruction.JMC, 0);

            new CaseVisitor().visit(incaseContext);

            Placeholder jmpPlaceholder = writer.writeInstruction(Instruction.JMP, 0);
            endJumps.add(jmpPlaceholder);

            jmcPlaceholder.fill(instructionsWritten);

        }

        for (Placeholder caseJ : endJumps) {
            caseJ.fill(instructionsWritten);
        }

        return null;
    }

    @Override
    public Void visitParallelassignStatement(GrammarParser.ParallelassignStatementContext ctx) {

        GrammarParser.ParallelassignContext multipassign = ctx.parallelassign();

        ExpressionVisitor expressionVisitor = new ExpressionVisitor();

        List<TerminalNode> idents = multipassign.IDENT();
        List<GrammarParser.ExpressionContext> expressions = multipassign.expression();

        if (idents.size() != expressions.size()) {
            throw new ParseCancellationException("Multiple arrays must have both arrays of the sae length.");
        }

        for (int i = 0; i < idents.size(); i++) {
            VariableType type = expressionVisitor.visit(expressions.get(i));
            if (type != VariableType.INT && type != VariableType.BOOL) {
                throw new ParseCancellationException(String.format(TYPE_ERROR_MSG, type, "INT or BOOL"));
            }

            String varName = idents.get(i).getSymbol().getText();

            Variable var = variableMap.get(currentFuncName, varName);

            if (var == null) {
                throw new ParseCancellationException(String.format(VAR_NOT_FOUND_ERROR_MSG, varName, currentFuncName));
            }

            if (var.getVariableType() == VariableType.CONSTINT) {
                throw new ParseCancellationException(String.format("Could not assign to constant variable %s.", varName));
            }

            VariableType exp = var.getVariableType();
            if (!((exp == VariableType.CONSTINT || exp == VariableType.INT) && (type == VariableType.INT)) && exp != type) {
                throw new ParseCancellationException(String.format("Variable %s type does not match with type of assigned expression.", varName));
            }

            writer.writeInstruction(Instruction.STO, currentLevel - var.getLevel(), var.getDeclarationIndex());
        }

        return null;
    }

    @Override
    public Void visitAssignStatement(GrammarParser.AssignStatementContext ctx) {
        GrammarParser.AssignContext assign = ctx.assign();
        return this.visit(assign);
    }

    @Override
    public Void visitAssign(GrammarParser.AssignContext ctx) {
        VariableType type = new ExpressionVisitor().visit(ctx.expression());

        if (type != VariableType.INT && type != VariableType.BOOL) {
            throw new ParseCancellationException(String.format(TYPE_ERROR_MSG, type, "INT or BOOL"));
        }

        String varName = ctx.IDENT().getSymbol().getText();

        Variable var = variableMap.get(currentFuncName, varName);

        if (var == null) {
            throw new ParseCancellationException(String.format(VAR_NOT_FOUND_ERROR_MSG, varName, currentFuncName));
        }

        if (var.getVariableType() == VariableType.CONSTINT) {
            throw new ParseCancellationException(String.format("Could not assign to constant variable %s.", varName));
        }

        VariableType exp = var.getVariableType();
        if (!((exp == VariableType.CONSTINT || exp == VariableType.INT) && (type == VariableType.INT)) && exp != type) {
            throw new ParseCancellationException(String.format("Variable %s type does not match with type of assigned expression.", varName));
        }

        writer.writeInstruction(Instruction.STO, currentLevel - var.getLevel(), var.getDeclarationIndex());
        return null;
    }
}
